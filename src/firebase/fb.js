import * as firebase from "firebase/app";

let FBCONFIG = {
  apiKey: `${process.env.REACT_APP_FB_APIKEY}`,
  authDomain: `${process.env.REACT_APP_FB_AUTHDOMAIN}`,
  databaseURL: `${process.env.REACT_APP_FB_DATABASE_URL}`,
  projectId: `${process.env.REACT_APP_FB_PROJECT_ID}`,
  storageBucket: `${process.env.REACT_APP_FB_STORAGE_BUCKET}`,
  messagingSenderId: `${process.env.REACT_APP_FB_MESSAGE_SENDER_ID}`
};

firebase.initializeApp(FBCONFIG);

export firebase;
