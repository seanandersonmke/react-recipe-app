import React, { Component } from "react";
import { Provider } from "react-redux";
import { createStore } from "redux";
import MainNav from "./components/navs/MainNav";
import AuthRoot from "./components/auth/AuthRoot";
import ViewContainer from "./components/global/ViewContainer";
import combineReducers from "./reducers/combineReducer";

const store = createStore(combineReducers);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MainNav />
        <AuthRoot />
        <ViewContainer />
      </Provider>
    );
  }
}

export default App;
