const viewReducer = (state = [], action) => {
  switch (action.type) {
    case "ADD_RECIPE_VIEW":
      return state.concat([action.data]);
    case "HOME_VIEW":
      return state.concat([action.data]);
    default:
      return state;
  }
};

export default viewReducer;
