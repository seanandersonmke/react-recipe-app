const recipes = (state = [], action) => {
  switch (action.type) {
    case "NEW_RECIPE":
      return state.concat([action.data]);
    default:
      return state;
  }
};

export default recipes;
