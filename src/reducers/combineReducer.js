import { combineReducers } from "redux";
import authReducer from "./authReducer";
import recipeReducer from "./recipeReducer";
import viewReducer from "./viewReducer";

export default combineReducers({
  authReducer,
  recipeReducer,
  viewReducer
});
