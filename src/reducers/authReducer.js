const authReducer = (state = [], action) => {
  switch (action.type) {
    case "LOGIN_STATUS":
      return state.concat([action.data]);
    default:
      return state;
  }
};

export default authReducer;
