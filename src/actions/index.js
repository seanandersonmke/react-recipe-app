let nextRecipeId = 0;

export const newRecipe = text => ({
  type: "NEW_RECIPE",
  id: nextRecipeId++,
  text
});

export const addRecipeView = view => ({
  type: "ADD_RECIPE_VIEW",
  id: nextRecipeId++,
  view
});

export const homeView = view => ({
  type: "HOME_VIEW",
  id: nextRecipeId++,
  view
});

export const loginStatus = status => ({
  type: "LOGIN_STATUS",
  status
});
