import React from "react";
import { connect } from "react-redux";

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      color: ""
    };
    this.input = React.createRef();
  }

  onSubmit = event => {
    event.preventDefault();
    let data = { value: this.input.current.value };

    this.props.dispatch({
      type: "NEW_RECIPE",
      data
    });
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <label>Blue</label>
          <input ref={this.input} type="radio" name="testradio" value="blue" />
          <label>Red</label>
          <input ref={this.input} type="radio" name="testradio" value="red" />
          <input type="submit" value="Submit" />
        </form>
        <div style={{ color: this.state.color }} />
      </div>
    );
  }
}

export default connect()(Home);
