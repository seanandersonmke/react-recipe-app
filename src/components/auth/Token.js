class Token {
  handleToken = user => {
    console.log("hit token");
    user.getIdToken().then(token => {
      if (!token) {
        return;
      }

      let baseurl = "http://localhost:8080/auth";

      return fetch(baseurl, {
        method: "POST",
        headers: {
          Accept: "application/json",
          "Content-Type": "application/json"
        },
        body: JSON.stringify({
          token: token
        })
      })
        .then(() => {
          return "Success";
        })
        .catch(error => {
          return error;
        });
    });
  };
}

export default Token;
