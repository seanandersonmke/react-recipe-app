import React from "react";
import { connect } from "react-redux";
import * as firebase from "firebase/app";
import "firebase/auth";
import Token from "./Token";

let FBCONFIG = {
  apiKey: `${process.env.REACT_APP_FB_APIKEY}`,
  authDomain: `${process.env.REACT_APP_FB_AUTHDOMAIN}`,
  databaseURL: `${process.env.REACT_APP_FB_DATABASE_URL}`,
  projectId: `${process.env.REACT_APP_FB_PROJECT_ID}`,
  storageBucket: `${process.env.REACT_APP_FB_STORAGE_BUCKET}`,
  messagingSenderId: `${process.env.REACT_APP_FB_MESSAGE_SENDER_ID}`
};

firebase.initializeApp(FBCONFIG);

class AuthRoot extends React.Component {
  token = new Token();

  constructor() {
    super();

    firebase.auth().onAuthStateChanged(user => {
      let data;
      if (user) {
        this.token.handleToken(user);
        data = { checkView: true };
      } else {
        data = { checkView: false };
      }

      this.props.dispatch({
        type: "LOGIN_STATUS",
        data
      });
    });
  }

  render() {
    return false;
  }
}

export default connect()(AuthRoot);
