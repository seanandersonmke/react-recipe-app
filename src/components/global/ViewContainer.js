import React from "react";
import Home from "../home/Home";
import Auth from "../auth/Auth";
import { connect } from "react-redux";

class ViewContainer extends React.Component {
  checkView = check => {
    if (this.props.state.authReducer.checkView === false) {
      return <Auth />;
    } else {
      return <Home />;
    }
  };

  render() {
    return <main>{this.checkView(true)}</main>;
  }
}

const mapStateToProps = state => {
  return {
    state: state
  };
};

export default connect(mapStateToProps)(ViewContainer);
