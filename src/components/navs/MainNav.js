import React from "react";
import { connect } from "react-redux";

class MainNav extends React.Component {
  logout = () => {
    console.log("will logout");
  };

  addRecipe = event => {
    event.preventDefault();
    this.props.dispatch({
      type: "ADD_RECIPE_VIEW"
    });
  };

  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="/">
          Recipe Book
        </a>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item active">
              <a className="nav-link" href="/">
                Home <span className="sr-only">(current)</span>
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/add" onClick={this.addRecipe}>
                Add Recipe
              </a>
            </li>
            <li className="nav-item">
              <a className="nav-link" href="/view">
                View Recipes
              </a>
            </li>
          </ul>
        </div>
        <a onClick={this.logout}>Logout</a>
      </nav>
    );
  }
}

export default connect()(MainNav);
